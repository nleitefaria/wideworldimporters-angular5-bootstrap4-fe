import { TestBed, inject } from '@angular/core/testing';

import { SpecialDealsService } from './special-deals.service';

describe('SpecialDealsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SpecialDealsService]
    });
  });

  it('should be created', inject([SpecialDealsService], (service: SpecialDealsService) => {
    expect(service).toBeTruthy();
  }));
});
