import { Component, OnInit } from '@angular/core';
import { BuyingGroupsService } from '../../services/buying-groups.service';

@Component({
  selector: 'app-buying-groups',
  templateUrl: './buying-groups.component.html',
  styleUrls: ['./buying-groups.component.css'],
  providers: [BuyingGroupsService]
})

export class BuyingGroupsComponent implements OnInit 
{
	buyingGroups: any;
    p: number = 1;
    total: number;
    loading: boolean;
    location: string = "Buying Groups"; 

  	constructor(private httpService : BuyingGroupsService) 
  	{ 	
  	}	

  	ngOnInit() 
  	{
  		this.init(); 	
  	}
  	
  	init()
    {  
		this.getPage(1);
    }
  	   
    getPage(page: number)
    {
    	this.loading = true;  
        this.httpService.getBuyingGroupsPage(page).subscribe(
			response =>{
				if(response.error) { 
					alert('Server Error');
				} else {								
					this.buyingGroups = response.content;
					this.p = page;					
					this.total = response.totalElements;
					this.loading = false;	  								
				}
			},
			error =>{
				alert('Server error');
			}
		);
    }

}
