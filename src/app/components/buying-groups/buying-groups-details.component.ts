import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'; 

import { BuyingGroupsService } from '../../services/buying-groups.service';

@Component({
  selector: 'app-buying-groups-details',
  templateUrl: './buying-groups-details.component.html',
  styleUrls: ['./buying-groups-details.component.css'],
  providers: [BuyingGroupsService]
})
export class BuyingGroupsDetailsComponent implements OnInit {

  	id: number;
	buyingGroups: any;

  	constructor(private route: ActivatedRoute, private httpService : BuyingGroupsService) 
  	{
  		this.route.params.subscribe((params) => 
  		{
  			this.id = params.id; 				
		});	
  	}

  	ngOnInit()
    {	
  		this.init(); 		 		
  	}
  	
  	init()
  	{		
  		this.httpService.getBuyingGroups(this.id).subscribe(
			response =>{
				if(response.error) { 
					alert('Server Error');
				} else {								
					this.buyingGroups = response;						
				}
			},
			error =>{
				alert('Server error');
			}
		);	
  	}

}
