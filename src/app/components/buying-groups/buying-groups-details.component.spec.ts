import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyingGroupsDetailsComponent } from './buying-groups-details.component';

describe('BuyingGroupsDetailsComponent', () => {
  let component: BuyingGroupsDetailsComponent;
  let fixture: ComponentFixture<BuyingGroupsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyingGroupsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyingGroupsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
