import { Component, OnInit } from '@angular/core';
import { SpecialDealsService } from '../../services/special-deals.service';

@Component({
  selector: 'app-special-deals',
  templateUrl: './special-deals.component.html',
  styleUrls: ['./special-deals.component.css'],
  providers: [SpecialDealsService]
})

export class SpecialDealsComponent implements OnInit
{
	specialDeals: any;
    p: number = 1;
    total: number;
    loading: boolean;
    location: string = "Special Deals"; 
	
  	constructor(private httpService : SpecialDealsService) 
  	{ 	
  	}	

  	ngOnInit() 
  	{
  		this.init(); 	
  	}
  	
  	init()
    {  
		this.getPage(1);
    }
  	   
    getPage(page: number)
    {
    	this.loading = true;  
        this.httpService.getSpecialDealsPage(page).subscribe(
			response =>{
				if(response.error) { 
					alert('Server Error');
				} else {								
					this.specialDeals = response.content;
					this.p = page;					
					this.total = response.totalElements;
					this.loading = false;	  								
				}
			},
			error =>{
				alert('Server error');
			}
		);
    }
}
