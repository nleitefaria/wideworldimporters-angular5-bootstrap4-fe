import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'; 

import { SpecialDealsService } from '../../services/special-deals.service';

@Component({
  selector: 'app-special-deals-details',
  templateUrl: './special-deals-details.component.html',
  styleUrls: ['./special-deals-details.component.css'],
  providers: [SpecialDealsService]
})

export class SpecialDealsDetailsComponent implements OnInit
{
	id: number;
	specialDeals: any;

  	constructor(private route: ActivatedRoute, private httpService : SpecialDealsService) 
  	{
  		this.route.params.subscribe((params) => 
  		{
  			this.id = params.id; 				
		});	
  	}

  	ngOnInit()
    {	
  		this.init(); 		 		
  	}
  	
  	init()
  	{		
  		this.httpService.getSpecialDeals(this.id).subscribe(
			response =>{
				if(response.error) { 
					alert('Server Error');
				} else {								
					this.specialDeals = response;						
				}
			},
			error =>{
				alert('Server error');
			}
		);	
  	}
}
