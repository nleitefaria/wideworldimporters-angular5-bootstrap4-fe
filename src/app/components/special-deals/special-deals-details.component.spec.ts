import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialDealsDetailsComponent } from './special-deals-details.component';

describe('SpecialDealsDetailsComponent', () => {
  let component: SpecialDealsDetailsComponent;
  let fixture: ComponentFixture<SpecialDealsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecialDealsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialDealsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
