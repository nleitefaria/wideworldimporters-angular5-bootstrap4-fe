import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuyingGroupsComponent } from './components/buying-groups/buying-groups.component';
import { BuyingGroupsDetailsComponent } from './components/buying-groups/buying-groups-details.component';
import { SpecialDealsComponent } from './components/special-deals/special-deals.component';
import { SpecialDealsDetailsComponent } from './components/special-deals/special-deals-details.component';

const routes: Routes = [
	{
		path: 'buying-groups',
		component: BuyingGroupsComponent
	},
	{
		path: 'buying-groups/:id',
		component: BuyingGroupsDetailsComponent
	},
	{
		path: 'special-deals',
		component: SpecialDealsComponent
	},
	{ 
		path: 'special-deals/:id', 
		component: SpecialDealsDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
