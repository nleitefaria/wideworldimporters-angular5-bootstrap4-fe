import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgxPaginationModule} from 'ngx-pagination';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { BuyingGroupsComponent } from './components/buying-groups/buying-groups.component';
import { SpecialDealsComponent } from './components/special-deals/special-deals.component';
import { SpecialDealsDetailsComponent } from './components/special-deals/special-deals-details.component';
import { BuyingGroupsDetailsComponent } from './components/buying-groups/buying-groups-details.component';


@NgModule({
  declarations: [
    AppComponent,
    BuyingGroupsComponent,
    SpecialDealsComponent,
    SpecialDealsDetailsComponent,
    BuyingGroupsDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
